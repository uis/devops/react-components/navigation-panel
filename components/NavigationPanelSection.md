### Examples

```js
import NavigationPanelSectionAnchor from "./NavigationPanelSectionAnchor";

<NavigationPanelSection dataRole="example-section-outer">
    <NavigationPanelSectionAnchor dataRole="example-section-anchor-1" link="#" text="Example section anchor 1"/>
    <NavigationPanelSectionAnchor dataRole="example-section-anchor-2" link="#" text="Example section anchor 2"/>
</NavigationPanelSection>
```