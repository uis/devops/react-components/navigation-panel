### Examples

```js

import NavigationPanelAvatar from "./NavigationPanelAvatar";
import NavigationPanelLogo from "./NavigationPanelLogo";
import NavigationPanelSection from "./NavigationPanelSection";
import NavigationPanelFooter from "./NavigationPanelFooter";
import NavigationPanelFooterLink from "./NavigationPanelFooterLink";
import NavigationPanelSectionLink from "./NavigationPanelSectionLink";
import NavigationPanelSectionAnchor from "./NavigationPanelSectionAnchor";

var profile = {
    displayName: "Mike Bamford",
    avatarUrl: "https://via.placeholder.com/40/86a3c3/444444",
    username: "mb2174"
};

var LogoImage = "https://www.hoart.cam.ac.uk/images/university-of-cambridge-logo/image_preview";

<NavigationPanel>
    <NavigationPanelLogo logoImage={LogoImage} logoImageAlt="Subject Moderation" projectStatusTag="Alpha" />
    <NavigationPanelAvatar profile={profile} />
    <NavigationPanelSection dataRole="example-section">
        <NavigationPanelSectionAnchor dataRole="example-anchor" link="#" text="Example Navigation Panel Anchor"/>
    </NavigationPanelSection>
    <NavigationPanelFooter>
        <NavigationPanelFooterLink link="https://gitlab.developers.cam.ac.uk/uis/devops/uga/smi/wikis/About" text="About"/>
    </NavigationPanelFooter>
</NavigationPanel>
```