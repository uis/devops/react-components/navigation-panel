### Examples

```js
import NavigationPanelFooter from "./NavigationPanelFooter";

<NavigationPanelFooter>
  <NavigationPanelFooterLink
    link="https://www.cam.ac.uk"
    text="Home"
  />
  <NavigationPanelFooterLink
    link="https://gitlab.developers.cam.ac.uk/uis/devops/react-components/navigation-panel"
    text="Contribute"
  />
</NavigationPanelFooter>
```