### Examples

```js
var LogoImage = "https://www.hoart.cam.ac.uk/images/university-of-cambridge-logo/image_preview";

<NavigationPanelLogo logoImage={LogoImage} logoImageAlt="Subject Moderation" projectStatusTag="Alpha" />
```