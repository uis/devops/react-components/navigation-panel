### Examples

```js
import { MemoryRouter, Route, Switch } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';

const LINKS = [
    {text: "Apples", to: '/apples', page: 'All about apples.'},
    {text: "Pears", to: '/pears', page: 'Interesting pear facts.'}
]

// The currently select link.
const [selected, setSelected] = React.useState();

<MemoryRouter>
    {
        LINKS.map(
            ({text, to}) => <NavigationPanelSectionLink
                id={to}
                text={text}
                selected={selected == to}
                to={to}
                dataRole='section-link'
            />
        )
    }

    <Switch>
        {
            LINKS.map(
                ({to, page}) => <Route 
                    id={to}
                    path={to}
                    render={({match: {path}}) => {
                        setSelected(path);
                        return <Typography variant="h5">{page}</Typography>
                    }}
                />
            )
        }
    </Switch>
</MemoryRouter>
```