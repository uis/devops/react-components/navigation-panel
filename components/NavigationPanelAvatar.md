### Examples

```js
var profile = {
    displayName: "Mike Bamford",
    avatarUrl: "https://via.placeholder.com/40/86a3c3/444444",
    username: "mb2174"
};

var profileNoImage = {
    displayName: "Mike Bamford (no image)",
    username: "mb2174"
};

<div>
    <NavigationPanelAvatar profile={profile} />
    <NavigationPanelAvatar profile={profileNoImage} />
</div>
```
